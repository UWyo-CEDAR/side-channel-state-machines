import pickle  # nosec B403
from typing import Tuple
import autosklearn.classification
from sklearn.model_selection import KFold
import sklearn.datasets
from sklearn.metrics import accuracy_score
from numpy import ndarray

# size constants for use in declaring our askl classifier
MB = 1
GB = 1024 * MB
MINUTE = 60
HOUR = 60 * MINUTE
DAY = 24 * HOUR

N_FOLDS = 4

JOBS = 24

def load_data() -> Tuple[ndarray, ndarray]:
    X, y = [], []
    with open("data/data.pkl", "rb") as f:
        X = pickle.load(f)  # nosec - B301
    with open("data/labels.pkl", "rb") as f:
        y = pickle.load(f)  # nosec - B301
    return (X, y)


def train_auto_pipeline(data, labels):
    automl = autosklearn.classification.AutoSklearnClassifier(
        time_left_for_this_task=int(18 * HOUR),
        per_run_time_limit=int(6 * HOUR),
        memory_limit=int(256 * GB / JOBS),
        n_jobs=JOBS,
        seed=1,
        tmp_folder="./logs/"
    )
    automl.fit(data, labels)
    return automl


def test_auto_pipeline(model, test_data, test_labels):
    predictions = model.predict(test_data)
    return accuracy_score(test_labels, predictions)


def main():

    all_data, all_labels = load_data()
    hyperfolds = KFold(n_splits=N_FOLDS)

    best_general_accuracy = 0.0
    most_general_model = None
    
    hyperfold = 0

    for data, validation_data in hyperfolds.split(all_data):
        hyperfold += 1
        print(f'Beginning hyperfold {hyperfold}')
        addressable_data = all_data[data]
        addressable_labels = all_labels[data]

        folds = KFold(n_splits=N_FOLDS)

        best_model = None
        best_accuracy = 0.0
        
        fold = 0
        
        for train, test in folds.split(addressable_data):
            fold += 1
            print(f'Beginning fold {fold}/{N_FOLDS}')
            ensemble = train_auto_pipeline(
                addressable_data[train], addressable_labels[train])
            accuracy = test_auto_pipeline(
                ensemble, addressable_data[test], addressable_labels[test]
            )
            if accuracy > best_accuracy:
                best_accuracy = accuracy
                best_model = ensemble
            print(accuracy)
            print(ensemble.leaderboard())

        general_accuracy = test_auto_pipeline(
            best_model, all_data[validation_data], all_labels[validation_data])
        print(general_accuracy)
        if general_accuracy > best_general_accuracy:
            best_general_accuracy = general_accuracy
            most_general_model = best_model

    print(f'The best models we found: acc={best_general_accuracy}')
    print(most_general_model.leaderboard())

    with open("best_model.pkl", "wb") as f:
        pickle.dump(most_general_model, f)

    print('Pickled best model as "best_model.pkl')


if __name__ == "__main__":
    main()
