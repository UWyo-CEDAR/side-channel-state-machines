from pprint import pprint
from typing import List
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score
from typing import Tuple

import pandas as pd
import numpy as np
import autokeras as ak
import tensorflow_addons as tfa

import pickle


def load_data() -> Tuple[np.ndarray, np.ndarray]:
    X, y = [], []
    with open("./data/data.pkl", "rb") as f:
        X = pickle.load(f)  # nosec - B301
    with open("./data/labels.pkl", "rb") as f:
        y = pickle.load(f)  # nosec - B301
    return (X, y)


def train_auto_pipeline(data, labels):
    automl = ak.StructuredDataClassifier(
        max_trials=1,
        objective="val_loss",
        seed=1
    )
    automl.fit(data, labels)
    return automl


def test_auto_pipeline(model, test_data, test_labels):
    predictions = model.predict(test_data)
    return accuracy_score(test_labels, predictions)

def main():
    X, y = load_data()

    hyper = StratifiedKFold(n_splits=4)
    for train_idx, val_idx in hyper.split(X, y):
        X_train, X_val = X[train_idx], X[val_idx]
        y_train, y_val = y[train_idx], y[val_idx]

        model = train_auto_pipeline(X_train, y_train)
        # keras uses baked in validation during the search
        accuracy = test_auto_pipeline(model, X_val, y_val)
        print(accuracy)
        model.export_model()

if __name__ == "__main__":
    main()