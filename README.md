# Auto Side Channel Analysis

Automated machine learning over SCA data.

*Project Lead:* Andey Robins (@andey-robins)

**Contributors**
- Clay Carper (@clay-carper)

## About

This repository houses some automated machine learning work set up over the data collected during our side channel attack research on program flow recovery ([Carper et al. 2022](https://ieeexplore.ieee.org/abstract/document/9978369)).

## Project Structure

## Setup and Installation

## Replication

## Contributing