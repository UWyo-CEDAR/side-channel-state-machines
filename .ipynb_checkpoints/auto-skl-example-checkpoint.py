#
# USE THIS FILE TO VERIFY YOUR SETUP
#
# if you're able to run this whole thing, you have the autoskl pipeline
# all set up and working on your machine!
#

import autosklearn.classification
import sklearn.model_selection
import sklearn.datasets
import sklearn.metrics

# size constants for use in declaring our askl classifier
MB = 1
GB = 1024 * MB
MINUTE = 60
HOUR = 60 * MINUTE
DAY = 24 * HOUR

if __name__ == "__main__":
    X, y = sklearn.datasets.load_digits(return_X_y=True)
    X_train, X_test, y_train, y_test = \
        sklearn.model_selection.train_test_split(X, y, random_state=1)
    automl = autosklearn.classification.AutoSklearnClassifier(
        time_left_for_this_task=30,
        memory_limit=10*GB,
        n_jobs=2,
    )
    automl.fit(X_train, y_train)
    y_hat = automl.predict(X_test)
    print("Accuracy score", sklearn.metrics.accuracy_score(y_test, y_hat))
    print(automl.leaderboard())

"""
Using this to debug the following error when we run this on the server 

ValueError: (" Dummy prediction failed with run state StatusType.MEMOUT and additional output: 
{'error': 'Memout (used more than 3072 MB).', 'configuration_origin': 'DUMMY'}.",)
"""
